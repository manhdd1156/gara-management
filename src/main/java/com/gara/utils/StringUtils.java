package com.gara.utils;

import javax.xml.bind.DatatypeConverter;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import com.gara.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtils {
    private static Logger logger = LoggerFactory.getLogger(StringUtils.class);

    public static String encryptMD5(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());

            byte[] byteData = md.digest();
            String myHash = DatatypeConverter
                    .printHexBinary(byteData).toLowerCase();
            return myHash;
        } catch (EnumConstantNotPresentException | NoSuchAlgorithmException e) {
            logger.error(e.getMessage(), e);
        }
        return "";
    }

    public static boolean checkPhone(String str) {
        // Bieu thuc chinh quy mo ta dinh dang so dien thoai
        String reg = "^(0|\\+84)(\\s|\\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\\d)(\\s|\\.)?(\\d{3})(\\s|\\.)?(\\d{3})$";

        // Kiem tra dinh dang
        boolean kt = str.matches(reg);

        if (kt == false) {
            return false;
        }
        return true;
    }

    public static boolean checkEmail(String str) {
        // Bieu thuc chinh quy mo ta dinh dang email
        String reg = "^(.+)@(\\\\S+)$";

        // Kiem tra dinh dang
        boolean kt = str.matches(reg);

        if (kt == false) {
            return false;
        }
        return true;
    }
}
