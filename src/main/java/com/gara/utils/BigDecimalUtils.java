package com.gara.utils;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Pattern;


public class BigDecimalUtils {

    public static boolean isIntegerValue(BigDecimal value) {
        return value.signum() == 0 || value.scale() <= 0 || value.stripTrailingZeros().scale() <= 0;
    }

    public static boolean isIntegerValue(String value) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        if (pattern.matcher(value).matches()) {
            System.out.println(value + " is Integer");
            return true;
        }
        System.out.println(value + " is not Integer");
        return false;
    }

    public static boolean isBetween(BigDecimal value, BigDecimal start, BigDecimal end) {
        if (Objects.isNull(value) || Objects.isNull(start) || Objects.isNull(end)) {
            return false;
        }

        return value.compareTo(start) >= 0 && value.compareTo(end) <= 0;
    }

}
