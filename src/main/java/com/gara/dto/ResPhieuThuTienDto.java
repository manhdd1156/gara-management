package com.gara.dto;

import com.gara.model.PhieuThuTienModel;
import lombok.AllArgsConstructor;
import lombok.Data;

import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ResPhieuThuTienDto extends Response {
    private List<PhieuThuTienModel> data;
}
