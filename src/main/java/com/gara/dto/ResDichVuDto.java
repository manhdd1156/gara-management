package com.gara.dto;

import com.gara.model.DichVuModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ResDichVuDto extends Response {
    private List<DichVuModel> data;
}
