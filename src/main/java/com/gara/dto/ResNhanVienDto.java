package com.gara.dto;

import com.gara.model.NhanVienModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ResNhanVienDto extends Response {
    private List<NhanVienModel> data;
}
