package com.gara.dto;

import com.gara.model.ChiTietPhieuSuaChuaModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ResPhieuSuaChuaDto extends Response {
    private List<ChiTietPhieuSuaChuaModel> data;
}
