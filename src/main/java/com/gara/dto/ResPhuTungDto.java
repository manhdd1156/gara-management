package com.gara.dto;

import com.gara.model.PhuTungModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ResPhuTungDto extends Response {
    private List<PhuTungModel> data;
}
