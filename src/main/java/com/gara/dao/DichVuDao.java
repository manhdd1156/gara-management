package com.gara.dao;


import com.gara.model.DichVuModel;

import java.util.List;

public interface DichVuDao {
    DichVuModel getDichVu(int id);

    List<DichVuModel> getDanhSachDichVu();

    int deleteDichVuById(String id);

    int updateDichVuById(String id, String tenDichVu, int tienCong);

    int insertDichVuInfo(String tenDichVu, int tienCong);
}
