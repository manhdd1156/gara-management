package com.gara.dao;


import com.gara.model.PhuTungCTPSCModel;
import com.gara.model.PhuTungModel;

import java.util.List;

public interface PhuTungDao {
    PhuTungModel getPhuTung(String id);

    List<PhuTungModel> getDanhSachPhuTung();

    List<PhuTungCTPSCModel> getDanhSachPhuTungByIdCTPSC(int id);

    int deletePhuTungById(String id);

    int updatePhuTungById(String id, String tenPhuTung, String mauSac, String moTa, int giaNhap, int giaBan, String maLoaiPT);

    int insertPhuTungInfo(String tenPhuTung, String mauSac, String moTa, int giaNhap, int giaBan, String maLoaiPT);
}
