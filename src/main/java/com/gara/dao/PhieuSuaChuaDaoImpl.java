package com.gara.dao;


import com.gara.model.ChiTietPhieuSuaChuaModel;
import com.gara.model.PhieuSuaChuaModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PhieuSuaChuaDaoImpl implements PhieuSuaChuaDao {

    private static final Logger logger = LogManager.getLogger(PhieuSuaChuaDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public ChiTietPhieuSuaChuaModel getChiTietPhieuSuaChua(String id) {
        ChiTietPhieuSuaChuaModel chiTietPhieuSuaChua = null;
        try {
            String sql = "SELECT * " +
                    "FROM chi_tiet_psc " +
                    "WHERE id = ? ";

            chiTietPhieuSuaChua = jdbcTemplate.queryForObject(sql, new Object[]{id},
                    (rs, rowNum) -> {
                        ChiTietPhieuSuaChuaModel item = ChiTietPhieuSuaChuaModel.builder()
                                .id(rs.getInt("id"))
                                .maPSC(rs.getInt("ma_psc"))
                                .maDV(rs.getInt("ma_dv"))
                                .ghichu(rs.getString("ghi_chu"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getChiTietPhieuSuaChua error", ex.getMessage());
        }
        return chiTietPhieuSuaChua;
    }

    @Override
    public List<ChiTietPhieuSuaChuaModel> getDanhSachChiTietPhieuSuaChua() {
        try {
            String sql = "SELECT * " +
                    "FROM chi_tiet_psc ";

            return jdbcTemplate.query(sql,
                    (rs, rowNum) -> {
                        ChiTietPhieuSuaChuaModel items = ChiTietPhieuSuaChuaModel.builder()
                                .id(rs.getInt("id"))
                                .maPSC(rs.getInt("ma_psc"))
                                .maDV(rs.getInt("ma_dv"))
                                .ghichu(rs.getString("ghi_chu"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachChiTietPhieuSuaChua error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public PhieuSuaChuaModel getPhieuSuaChua(int id) {
        PhieuSuaChuaModel phieuSuaChua = null;
        try {
            String sql = "SELECT * " +
                    "FROM phieu_sua_chua " +
                    "WHERE id = ? ";

            phieuSuaChua = jdbcTemplate.queryForObject(sql, new Object[]{id},
                    (rs, rowNum) -> {
                        PhieuSuaChuaModel item = PhieuSuaChuaModel.builder()
                                .id(rs.getInt("id"))
                                .bienSo(rs.getString("bien_so_xe"))
                                .ngaySuaChua(rs.getDate("ngay_sua_chua"))
                                .maNV(rs.getString("ma_nv"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getPhieuSuaChua error", ex.getMessage());
        }
        return phieuSuaChua;
    }

    @Override
    public List<PhieuSuaChuaModel> getDanhSachPhieuSuaChua() {
        try {
            String sql = "SELECT * " +
                    "FROM phieu_sua_chua ";

            return jdbcTemplate.query(sql,
                    (rs, rowNum) -> {
                        PhieuSuaChuaModel items = PhieuSuaChuaModel.builder()
                                .id(rs.getInt("id"))
                                .bienSo(rs.getString("bien_so_xe"))
                                .ngaySuaChua(rs.getDate("ngay_sua_chua"))
                                .maNV(rs.getString("ma_nv"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachPhieuSuaChua error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public int insertPhieuSuaChuaInfo(String bienSo, String ngaySuaChua, String maNV,String ghichu,String maDV) {
        try {
            String sql = "INSERT INTO phieu_sua_chua(bien_so_xe, ngay_sua_chua, ma_nv)"
                    + " VALUES ('" + bienSo +"','" + ngaySuaChua + "','"+maNV+"')";
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlFirst = sql;
            int insertsCount =  jdbcTemplate.update(c -> c.prepareStatement(sqlFirst, Statement.RETURN_GENERATED_KEYS), keyHolder);
            if (insertsCount == 1) {
                Number assignedKey = keyHolder.getKey();
                long rowId = assignedKey.longValue();
                System.out.println("rowId = " + rowId);
                sql = "INSERT INTO chi_tiet_psc(ma_psc,ma_dv,ghi_chu)"
                        + " VALUES ('" + rowId  + "','"+maDV+ "','"+ghichu+"')";
                insertsCount = jdbcTemplate.update(sql);
            }
            return insertsCount;
        } catch (Exception e) {
            logger.error("insertPhieuSuaChuaInfo error:" + e.getMessage(), e);
            return -1;
        }
    }

    @Override
    public int updatePhieuSuaChuaById(String id, String bienSo, String ngaySuaChua, String maNV,String maPSC,String ghichu,String maDV) {
        logger.info("Start to updatePhieuSuaChuaById: {}", id);
        try {
            String sql = "UPDATE chi_tiet_psc SET ma_dv=?, ghi_chu=? " +
                    "WHERE id=? ";
            int result = jdbcTemplate.update(sql, maDV,ghichu, id);
            if(result==1) {
                sql = "UPDATE phieu_sua_chua SET bien_so_xe=?, ngay_sua_chua=?, ma_nv=? " +
                        "WHERE id=? ";
            }
            return jdbcTemplate.update(sql, bienSo, ngaySuaChua, maNV, maPSC);
        } catch (Exception e) {
            logger.error("updatePhieuSuaChuaById: ", e);
            return -1;
        }
    }

    @Override
    public int deletePhieuSuaChuaById(String id) {
        logger.info("Start to deletePhieuSuaChuaById: {}", id);

        String sql = "DELETE FROM phieu_sua_chua " +
                "WHERE id = ? ";

        int ret = jdbcTemplate.update(sql, id);

        logger.info("deletePhieuSuaChuaById success with id: {}", id);

        return ret;
    }

}