package com.gara.dao;


import com.gara.model.NhanVienModel;
import com.gara.model.NhanVienModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class NhanVienDaoImpl implements NhanVienDao {

    private static final Logger logger = LogManager.getLogger(NhanVienDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public NhanVienModel getNhanVien(String id) {
        NhanVienModel nhanVien = null;
        try {
            String sql = "SELECT * " +
                    "FROM nhan_vien " +
                    "WHERE id = ? ";

            nhanVien = jdbcTemplate.queryForObject(sql, new Object[]{id},
                    (rs, rowNum) -> {
                        NhanVienModel item = NhanVienModel.builder()
                                .id(rs.getInt("id"))
                                .hoTen(rs.getString("ho_ten"))
                                .sdt(rs.getString("sdt"))
                                .diaChi(rs.getString("dia_chi"))
                                .maUser(rs.getString("ma_user"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getNhanVien error", ex.getMessage());
        }
        return nhanVien;
    }

    @Override
    public NhanVienModel getNhanVienByHoTen(String hoTen) {
        NhanVienModel khachHang = null;
        try {
            String sql = "SELECT * " +
                    "FROM nhan_vien " +
                    "WHERE ho_ten like ? ";

            khachHang = jdbcTemplate.queryForObject(sql, new Object[]{"%"+hoTen +"%"},
                    (rs, rowNum) -> {
                        NhanVienModel item = NhanVienModel.builder()
                                .id(rs.getInt("id"))
                                .hoTen(rs.getString("ho_ten"))
                                .sdt(rs.getString("sdt"))
                                .diaChi(rs.getString("dia_chi"))
                                .maUser(rs.getString("ma_nguoi_dung"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getNhanVienByHoTen error", ex.getMessage());
        }
        return khachHang;
    }
    
    @Override
    public List<NhanVienModel> getDanhSachNhanVien() {
        try {
            String sql = "SELECT * " +
                    "FROM nhan_vien " +
                    "ORDER BY ho_ten DESC ";

            return jdbcTemplate.query(sql,
                    (rs, rowNum) -> {
                        NhanVienModel items = NhanVienModel.builder()
                                .id(rs.getInt("id"))
                                .hoTen(rs.getString("ho_ten"))
                                .sdt(rs.getString("sdt"))
                                .diaChi(rs.getString("dia_chi"))
                                .maUser(rs.getString("ma_nguoi_dung"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachNhanVien error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public int insertNhanVienInfo(String hoTen, String sdt, String diaChi, String maUser) {
        try {
            String sql = "INSERT INTO nhan_vien(ho_ten,sdt, dia_chi, ma_nguoi_dung)"
                    + " VALUES (?, ?, ?,?)";

            return jdbcTemplate.update(sql, hoTen, sdt, diaChi, maUser);
        } catch (Exception e) {
            logger.error("insertNhanVienInfo error:" + e.getMessage(), e);
            return -1;
        }
    }

    @Override
    public int updateNhanVienById(String id, String hoTen, String sdt, String diaChi, String maUser) {
        logger.info("Start to updateNhanVienById: {}", id);
        try {
            String sql = "UPDATE nhan_vien SET ho_ten=?,sdt=?, dia_chi=?, ma_nguoi_dung=? " +
                    "WHERE id=? ";

            return jdbcTemplate.update(sql, hoTen, sdt, diaChi, maUser, id);
        } catch (Exception e) {
            logger.error("updateNhanVienById: ", e);
            return -1;
        }
    }

    @Override
    public int deleteNhanVienById(String id) {
        logger.info("Start to deleteNhanVienById: {}", id);

        String sql = "DELETE FROM nhan_vien " +
                "WHERE id = ? ";

        int ret = jdbcTemplate.update(sql, id);

        logger.info("deleteNhanVienById success with id: {}", id);

        return ret;
    }

}