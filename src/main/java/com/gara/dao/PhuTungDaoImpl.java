package com.gara.dao;


import com.gara.model.PhuTungCTPSCModel;
import com.gara.model.PhuTungModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PhuTungDaoImpl implements PhuTungDao {

    private static final Logger logger = LogManager.getLogger(PhuTungDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public PhuTungModel getPhuTung(String id) {
        PhuTungModel nhanVien = null;
        try {
            String sql = "SELECT * " +
                    "FROM phu_tung " +
                    "WHERE id = ? ";

            nhanVien = jdbcTemplate.queryForObject(sql, new Object[]{id},
                    (rs, rowNum) -> {
                        PhuTungModel item = PhuTungModel.builder()
                                .id(rs.getInt("id"))
                                .tenPhuTung(rs.getString("ten_phu_tung"))
                                .mauSac(rs.getString("mau_sac"))
                                .moTa(rs.getString("mo_ta"))
                                .giaNhap(rs.getInt("gia_nhap"))
                                .giaBan(rs.getInt("gia_ban"))
                                .maLoaiPT(rs.getString("ma_loai_pt"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getPhuTung error", ex.getMessage());
        }
        return nhanVien;
    }

    @Override
    public List<PhuTungModel> getDanhSachPhuTung() {
        try {
            String sql = "SELECT * " +
                    "FROM phu_tung " +
                    "ORDER BY ten_phu_tung DESC ";

            return jdbcTemplate.query(sql,
                    (rs, rowNum) -> {
                        PhuTungModel items = PhuTungModel.builder()
                                .id(rs.getInt("id"))
                                .tenPhuTung(rs.getString("ten_phu_tung"))
                                .mauSac(rs.getString("mauSac"))
                                .moTa(rs.getString("mo_ta"))
                                .giaNhap(rs.getInt("gia_nhap"))
                                .giaBan(rs.getInt("gia_ban"))
                                .maLoaiPT(rs.getString("ma_loai_pt"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachPhuTung error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public List<PhuTungCTPSCModel> getDanhSachPhuTungByIdCTPSC(int id) {
        try {
            String sql = "SELECT pt.*,pt_ctpsc.id AS pt_ctpsc_id, pt_ctpsc.so_luong AS so_luong_su_dung " +
                    "FROM phu_tung pt " +
                    "JOIN phutung_ctpsc  pt_ctpsc " +
                    "ON pt.id = pt_ctpsc.ma_pt " +
                    "WHERE pt_ctpsc.ma_ctpsc = ?";

            return jdbcTemplate.query(sql,new Object[]{id},
                    (rs, rowNum) -> {
                        PhuTungCTPSCModel items = PhuTungCTPSCModel.builder()
                                .id(rs.getInt("pt_ctpsc_id"))
                                .maPT(rs.getInt("id"))
                                .phuTung(new PhuTungModel(rs.getInt("id"),rs.getString("ten_phu_tung"),
                                        rs.getString("mau_sac"),rs.getString("mo_ta"),rs.getInt("gia_nhap"),
                                        rs.getInt("gia_ban"),rs.getInt("so_luong"),rs.getString("ma_loai_pt")))
                                .soLuongSuDung(rs.getInt("so_luong_su_dung"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachPhuTungByIdCTPSC error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public int insertPhuTungInfo(String tenPhuTung, String mauSac, String moTa, int giaNhap, int giaBan, String maLoaiPT) {
        try {
            String sql = "INSERT INTO phu_tung(ten_phu_tung,mauSac, mo_ta,gia_nhap,gia_ban, ma_loai_pt)"
                    + " VALUES (?,?,?,?,?,?)";

            return jdbcTemplate.update(sql, tenPhuTung, mauSac, moTa, giaNhap, giaBan, maLoaiPT);
        } catch (Exception e) {
            logger.error("insertPhuTungInfo error:" + e.getMessage(), e);
            return -1;
        }
    }

    @Override
    public int updatePhuTungById(String id, String tenPhuTung, String mauSac, String moTa, int giaNhap, int giaBan, String maLoaiPT) {
        logger.info("Start to updatePhuTungById: {}", id);
        try {
            String sql = "UPDATE phu_tung SET ten_phu_tung=?,mau_sac=?, mo_ta=?,gia_nhap,gia_ban, ma_loai_pt=? " +
                    "WHERE id=? ";

            return jdbcTemplate.update(sql, tenPhuTung, mauSac, moTa, giaNhap, giaBan, maLoaiPT, id);
        } catch (Exception e) {
            logger.error("updatePhuTungById: ", e);
            return -1;
        }
    }

    @Override
    public int deletePhuTungById(String id) {
        logger.info("Start to deletePhuTungById: {}", id);

        String sql = "DELETE FROM phu_tung " +
                "WHERE id = ? ";

        int ret = jdbcTemplate.update(sql, id);

        logger.info("deletePhuTungById success with id: {}", id);

        return ret;
    }

}