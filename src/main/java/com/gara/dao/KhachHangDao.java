package com.gara.dao;


import com.gara.model.KhachHangModel;

import java.util.List;

public interface KhachHangDao {
    KhachHangModel getKhachHang(String id);

    KhachHangModel getKhachHangByHoTen(String hoTen);

    List<KhachHangModel> getDanhSachKhachHang();

    int deleteKhachHangById(String id);

    int updateKhachHangById(String id, String hoTen, String sdt, String diaChi, String email);

    int insertKhachHangInfo(String hoTen, String sdt, String diaChi, String email);
}
