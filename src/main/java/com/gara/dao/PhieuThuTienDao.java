package com.gara.dao;


import com.gara.model.PhieuThuTienModel;

import java.util.List;

public interface PhieuThuTienDao {
    PhieuThuTienModel getPhieuThuTien(String id);

    List<PhieuThuTienModel> getDanhSachPhieuThuTien();

    int deletePhieuThuTienById(String id);

    int updatePhieuThuTienById(String id, String bienSo, String ngayThuTien, int soTienThu, String maNV);

    int insertPhieuThuTienInfo(String bienSo, String ngayThuTien, int soTienThu, String maNV);
}
