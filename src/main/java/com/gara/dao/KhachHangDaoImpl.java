package com.gara.dao;


import com.gara.model.KhachHangModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class KhachHangDaoImpl implements KhachHangDao {

    private static final Logger logger = LogManager.getLogger(KhachHangDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public KhachHangModel getKhachHang(String id) {
        KhachHangModel khachHang = null;
        try {
            String sql = "SELECT * " +
                    "FROM khach_hang " +
                    "WHERE id = ? ";

            khachHang = jdbcTemplate.queryForObject(sql, new Object[]{id},
                    (rs, rowNum) -> {
                        KhachHangModel item = KhachHangModel.builder()
                                .id(rs.getInt("id"))
                                .hoTen(rs.getString("ho_ten"))
                                .sdt(rs.getString("sdt"))
                                .diaChi(rs.getString("dia_chi"))
                                .email(rs.getString("email"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getKhachHang error", ex.getMessage());
        }
        return khachHang;
    }

    @Override
    public KhachHangModel getKhachHangByHoTen(String hoTen) {
        KhachHangModel khachHang = null;
        try {
            String sql = "SELECT * " +
                    "FROM khach_hang " +
                    "WHERE ho_ten like ? ";

            khachHang = jdbcTemplate.queryForObject(sql, new Object[]{"%"+hoTen +"%"},
                    (rs, rowNum) -> {
                        KhachHangModel item = KhachHangModel.builder()
                                .id(rs.getInt("id"))
                                .hoTen(rs.getString("ho_ten"))
                                .sdt(rs.getString("sdt"))
                                .diaChi(rs.getString("dia_chi"))
                                .email(rs.getString("email"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getKhachHangByHoTen error", ex.getMessage());
        }
        return khachHang;
    }

    @Override
    public List<KhachHangModel> getDanhSachKhachHang() {
        try {
            String sql = "SELECT * " +
                    "FROM khach_hang " +
                    "ORDER BY ho_ten DESC ";

            return jdbcTemplate.query(sql,
                    (rs, rowNum) -> {
                        KhachHangModel items = KhachHangModel.builder()
                                .id(rs.getInt("id"))
                                .hoTen(rs.getString("ho_ten"))
                                .sdt(rs.getString("sdt"))
                                .diaChi(rs.getString("dia_chi"))
                                .email(rs.getString("email"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachKhachHang error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public int insertKhachHangInfo(String hoTen, String sdt, String diaChi, String email) {
        try {
            String sql = "INSERT INTO khach_hang(ho_ten,sdt, dia_chi, email)"
                    + " VALUES (?, ?, ?,?)";

            return jdbcTemplate.update(sql, hoTen, sdt, diaChi, email);
        } catch (Exception e) {
            logger.error("insertKhachHangInfo error:" + e.getMessage(), e);
            return -1;
        }
    }

    @Override
    public int updateKhachHangById(String id, String hoTen, String sdt, String diaChi, String email) {
        logger.info("Start to updateKhachHangById: {}", id);
        try {
            String sql = "UPDATE khach_hang SET ho_ten=?,sdt=?, dia_chi=?, email=? " +
                    "WHERE id=? ";

            return jdbcTemplate.update(sql, hoTen, sdt, diaChi, email, id);
        } catch (Exception e) {
            logger.error("updateKhachHangById: ", e);
            return -1;
        }
    }

    @Override
    public int deleteKhachHangById(String id) {
        logger.info("Start to deleteKhachHangById: {}", id);

        String sql = "DELETE FROM khach_hang " +
                "WHERE id = ? ";

        int ret = jdbcTemplate.update(sql, id);

        logger.info("deleteKhachHangById success with id: {}", id);

        return ret;
    }

}