package com.gara.dao;


import com.gara.model.ChiTietPhieuSuaChuaModel;
import com.gara.model.PhieuSuaChuaModel;

import java.util.List;

public interface PhieuSuaChuaDao {
    ChiTietPhieuSuaChuaModel getChiTietPhieuSuaChua(String id);

    List<ChiTietPhieuSuaChuaModel> getDanhSachChiTietPhieuSuaChua();

    PhieuSuaChuaModel getPhieuSuaChua(int id);

    List<PhieuSuaChuaModel> getDanhSachPhieuSuaChua();

    int deletePhieuSuaChuaById(String id);

    int updatePhieuSuaChuaById(String id, String bienSo, String ngaySuaChua, String maNV,String maPSC,String ghichu,String maDV);

    int insertPhieuSuaChuaInfo(String bienSo, String ngaySuaChua, String maNV,String ghichu,String maDV);
}
