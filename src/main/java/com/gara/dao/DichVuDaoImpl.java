package com.gara.dao;


import com.gara.model.DichVuModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class DichVuDaoImpl implements DichVuDao {

    private static final Logger logger = LogManager.getLogger(DichVuDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public DichVuModel getDichVu(int id) {
        DichVuModel nhanVien = null;
        try {
            String sql = "SELECT * " +
                    "FROM dich_vu " +
                    "WHERE id = ? ";

            nhanVien = jdbcTemplate.queryForObject(sql, new Object[]{id},
                    (rs, rowNum) -> {
                        DichVuModel item = DichVuModel.builder()
                                .id(rs.getInt("id"))
                                .tenDichVu(rs.getString("ten_dich_vu"))
                                .tienCong(rs.getInt("tien_cong"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getDichVu error", ex.getMessage());
        }
        return nhanVien;
    }

    @Override
    public List<DichVuModel> getDanhSachDichVu() {
        try {
            String sql = "SELECT * " +
                    "FROM dich_vu " +
                    "ORDER BY ten_dich_vu DESC ";

            return jdbcTemplate.query(sql,
                    (rs, rowNum) -> {
                        DichVuModel items = DichVuModel.builder()
                                .id(rs.getInt("id"))
                                .tenDichVu(rs.getString("ten_dich_vu"))
                                .tienCong(rs.getInt("tien_cong"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachDichVu error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public int insertDichVuInfo(String tenDichVu, int tienCong) {
        try {
            String sql = "INSERT INTO dich_vu(ten_dich_vu, tien_cong)"
                    + " VALUES (?,?)";

            return jdbcTemplate.update(sql, tenDichVu, tienCong);
        } catch (Exception e) {
            logger.error("insertDichVuInfo error:" + e.getMessage(), e);
            return -1;
        }
    }

    @Override
    public int updateDichVuById(String id, String tenDichVu, int tienCong) {
        logger.info("Start to updateDichVuById: {}", id);
        try {
            String sql = "UPDATE dich_vu SET ten_dich_vu=?,tien_cong=? " +
                    "WHERE id=? ";

            return jdbcTemplate.update(sql, tenDichVu,tienCong, id);
        } catch (Exception e) {
            logger.error("updateDichVuById: ", e);
            return -1;
        }
    }

    @Override
    public int deleteDichVuById(String id) {
        logger.info("Start to deleteDichVuById: {}", id);

        String sql = "DELETE FROM dich_vu " +
                "WHERE id = ? ";

        int ret = jdbcTemplate.update(sql, id);

        logger.info("deleteDichVuById success with id: {}", id);

        return ret;
    }

}