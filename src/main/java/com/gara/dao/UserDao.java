package com.gara.dao;


import com.gara.model.UserModel;

public interface UserDao {
    UserModel getExistedUser(String username, String password);
}
