package com.gara.dao;


import com.gara.model.XeModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class XeDaoImpl implements XeDao {

    private static final Logger logger = LogManager.getLogger(XeDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public XeModel getXe(String bienSo) {
        XeModel xe = null;
        try {
            String sql = "SELECT * " +
                    "FROM xe " +
                    "WHERE bien_so = ? ";

            xe = jdbcTemplate.queryForObject(sql, new Object[]{bienSo},
                    (rs, rowNum) -> {
                        XeModel item = XeModel.builder()
                                .id(rs.getInt("id"))
                                .bienSo(rs.getString("bien_so"))
                                .tenXe(rs.getString("ten_xe"))
                                .maHangXe(rs.getString("ma_hang_xe"))
                                .tinhTrang(rs.getString("tinh_trang"))
                                .mauSac(rs.getString("mau_sac"))
                                .maKH(rs.getString("ma_kh"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getXe error", ex.getMessage());
        }
        return xe;
    }

    @Override
    public List<XeModel> getDanhSachXe() {
        try {
            String sql = "SELECT * " +
                    "FROM xe " +
                    "ORDER BY ten_xe DESC ";

            return jdbcTemplate.query(sql,
                    (rs, rowNum) -> {
                        XeModel items = XeModel.builder()
                                .id(rs.getInt("id"))
                                .bienSo(rs.getString("bien_so"))
                                .tenXe(rs.getString("ten_xe"))
                                .maHangXe(rs.getString("ma_hang_xe"))
                                .tinhTrang(rs.getString("tinh_trang"))
                                .mauSac(rs.getString("mau_sac"))
                                .maKH(rs.getString("ma_kh"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachXe error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public int insertXeInfo(String bienSo, String tenXe, String maHangXe, String tinhTrang, String mauSac, int maKH) {
        try {
            String sql = "INSERT INTO xe(bien_so,ten_xe, ma_hang_xe, tinh_trang,mau_sac,ma_kh )"
                    + " VALUES (?,?,?,?,?,?)";

            return jdbcTemplate.update(sql, bienSo, tenXe,  maHangXe,  tinhTrang,  mauSac,  maKH);
        } catch (Exception e) {
            logger.error("insertXeInfo error:" + e.getMessage(), e);
            return -1;
        }
    }

    @Override
    public int updateXeByBienSo(String bienSo, String tenXe, String maHangXe, String tinhTrang, String mauSac, int maKH) {
        logger.info("Start to updateXeByBienSo: {}", bienSo);
        try {
            String sql = "UPDATE xe SET ten_xe=?, ma_hang_xe=?, tinh_trang=?,mau_sac=?,ma_kh=? " +
                    "WHERE bien_so=? ";

            return jdbcTemplate.update(sql, bienSo, tenXe,  maHangXe,  tinhTrang,  mauSac,  maKH, bienSo);
        } catch (Exception e) {
            logger.error("updateXeByBienSo: ", e);
            return -1;
        }
    }

    @Override
    public int deleteXeByBienSo(String bienSo) {
        logger.info("Start to deleteXeByBienSo: {}", bienSo);

        String sql = "DELETE FROM xe " +
                "WHERE bien_so = ? ";

        int ret = jdbcTemplate.update(sql, bienSo);

        logger.info("deleteXeByBienSo success with id: {}", bienSo);

        return ret;
    }

}