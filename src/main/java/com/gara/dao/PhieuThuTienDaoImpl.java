package com.gara.dao;


import com.gara.model.PhieuThuTienModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

@Repository
public class PhieuThuTienDaoImpl implements PhieuThuTienDao {

    private static final Logger logger = LogManager.getLogger(PhieuThuTienDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public PhieuThuTienModel getPhieuThuTien(String id) {
        PhieuThuTienModel phieuthutien = null;
        try {
            String sql = "SELECT * " +
                    "FROM phieu_thu_tien " +
                    "WHERE id = ? ";

            phieuthutien = jdbcTemplate.queryForObject(sql, new Object[]{id},
                    (rs, rowNum) -> {
                        PhieuThuTienModel item = PhieuThuTienModel.builder()
                                .id(rs.getInt("id"))
                                .bienSo(rs.getString("bien_so_xe"))
                                .ngayThuTien(rs.getDate("ngay_thu_tien"))
                                .soTienThu(rs.getInt("so_tien_thu"))
                                .maNV(rs.getString("ma_nv"))
                                .build();
                        return item;
                    });
        } catch (Exception ex) {
            logger.error("getPhieuThuTien error", ex.getMessage());
        }
        return phieuthutien;
    }

    @Override
    public List<PhieuThuTienModel> getDanhSachPhieuThuTien() {
        try {
            String sql = "SELECT * " +
                    "FROM phieu_thu_tien " +
                    "ORDER BY ngay_thu_tien DESC ";

            return jdbcTemplate.query(sql,
                    (rs, rowNum) -> {
                        PhieuThuTienModel items = PhieuThuTienModel.builder()
                                .id(rs.getInt("id"))
                                .bienSo(rs.getString("bien_so_xe"))
                                .ngayThuTien(rs.getDate("ngay_thu_tien"))
                                .soTienThu(rs.getInt("so_tien_thu"))
                                .maNV(rs.getString("ma_nv"))
                                .build();
                        return items;
                    });
        } catch (Exception ex) {
            logger.error("getDanhSachPhieuThuTien error", ex.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public int insertPhieuThuTienInfo(String bienSo, String ngayThuTien,
                                      int soTienThu, String maNV) {
        try {
            String sql = "INSERT INTO phieu_thu_tien(bien_so_xe, ngay_thu_tien, so_tien_thu, ma_nv)"
                    + " VALUES (?, ?, ?, ?)";

            return jdbcTemplate.update(sql, bienSo, ngayThuTien, soTienThu, maNV);
        } catch (Exception e) {
            logger.error("insertPhieuThuTienInfo error:" + e.getMessage(), e);
            return -1;
        }
    }

    @Override
    public int updatePhieuThuTienById(String id, String bienSo, String ngayThuTien,
                                      int soTienThu, String maNV) {
        logger.info("Start to updatePhieuThuTienById: {}", id);
        try {
            String sql = "UPDATE phieu_thu_tien SET bien_so=?, ngay_thu_tien=?, " +
                    " so_tien_thu=?, ma_nv=? " +
                    "WHERE id=? ";

            return jdbcTemplate.update(sql, bienSo, ngayThuTien, soTienThu, maNV, id);
        } catch (Exception e) {
            logger.error("updatePhieuThuTienById: ", e);
            return -1;
        }
    }

    @Override
    public int deletePhieuThuTienById(String id) {
        logger.info("Start to deletePhieuThuTienById: {}", id);

        String sql = "DELETE FROM phieu_thu_tien " +
                "WHERE id = ? ";

        int ret = jdbcTemplate.update(sql, id);

        logger.info("deletePhieuThuTienById success with id: {}", id);

        return ret;
    }

}