package com.gara.api;

import com.gara.dto.ResNhanVienDto;
import com.gara.dto.ResNhanVienDto;
import com.gara.model.NhanVienModel;
import com.gara.model.NhanVienModel;
import com.gara.service.NhanVienService;
import com.gara.utils.BigDecimalUtils;
import com.gara.utils.DateUtils;
import com.gara.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/nhanvien")
@AllArgsConstructor
public class NhanVienController {
    private static Logger logger = LoggerFactory.getLogger(NhanVienController.class);
    @Autowired
    private NhanVienService nhanVienService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> get(@PathVariable String id) {
        try {
            logger.info("#getNhanVien - Start - id :" + id);
            List<NhanVienModel> listNhanVien = new ArrayList<NhanVienModel>();
            NhanVienModel nhanVien = nhanVienService.getNhanVien(id);
            if (nhanVien == null) {
                return ResponseEntity.status(404).body(ResNhanVienDto.builder().code(404).message("Not Found").build());
            }
            listNhanVien.add(nhanVien);
            ResNhanVienDto resNhanVienDto = ResNhanVienDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listNhanVien)
                    .build();

            return ResponseEntity.ok().body(resNhanVienDto);
        } catch (Exception e) {
            logger.info("request getNhanVien: ", e);
            throw e;
        }
    }

    @GetMapping(value = "/search")
    public ResponseEntity<?> getByName(@RequestParam("hoTen") @NotBlank String hoTen) {
        try {
            logger.info("#getNhanVienByHoTen - Start :" + hoTen);
            List<NhanVienModel> listNhanVien = new ArrayList<NhanVienModel>();
            NhanVienModel nhanVien = nhanVienService.getNhanVienByHoTen(hoTen);
            if (nhanVien == null) {
                return ResponseEntity.status(404).body(ResNhanVienDto.builder().code(404).message("Not Found").build());
            }
            listNhanVien.add(nhanVien);
            ResNhanVienDto resNhanVienDto = ResNhanVienDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listNhanVien)
                    .build();

            return ResponseEntity.ok().body(resNhanVienDto);
        } catch (Exception e) {
            logger.info("request getNhanVienByHoTen: ", e);
            throw e;
        }
    }

    @GetMapping(value = "")
    public ResponseEntity<?> getAll() {
        try {
            logger.info("#getAllNhanVien - Start:");
            List<NhanVienModel> listNhanVien = nhanVienService.getDanhSachNhanVien();
            ResNhanVienDto resNhanVienDto = ResNhanVienDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listNhanVien)
                    .build();
            logger.info("#getAllNhanVien - Response:" + resNhanVienDto.toString());
            return ResponseEntity.ok().body(resNhanVienDto);
        } catch (Exception e) {
            logger.info("request getAllNhanVien: ", e);
            throw e;
        }
    }

    @PostMapping(value = "/reg-info")
    @ResponseBody
    public ResponseEntity<?> regNhanVienInfo(
            @RequestParam("hoTen") @NotBlank String hoTen,
            @RequestParam("diaChi") @NotBlank String diaChi,
            @RequestParam("sdt") @NotNull String sdt,
            @RequestParam("maUser") @NotBlank String maUser
    ) {
        logger.info("request regnhanVienInfo");
        try {
            if (!StringUtils.checkPhone(sdt) || !BigDecimalUtils.isIntegerValue(maUser)) {
                return ResponseEntity.unprocessableEntity().body(ResNhanVienDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = nhanVienService.insertNhanVienInfo(hoTen, sdt, diaChi, maUser);

            if (result == 0) {
                return ResponseEntity.status(202).body(ResNhanVienDto.builder().code(202).message("Insert Failed").build());
            }

            ResNhanVienDto response = ResNhanVienDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request regNhanVienInfo: " + hoTen + "," + diaChi + "," + sdt + "," + maUser, e);
            throw e;
        }
    }

    @PostMapping(value = "/update/{id}")
    @ResponseBody
    public ResponseEntity<?> updateNhanVienById(
            @PathVariable String id,
            @RequestParam("hoTen") @NotBlank String hoTen,
            @RequestParam("diaChi") @NotBlank String diaChi,
            @RequestParam("sdt") @NotNull String sdt,
            @RequestParam("maUser") @NotBlank String maUser
    ) {
        logger.info("request updateNhanVienById");
        try {
            if (!StringUtils.checkPhone(sdt) || !BigDecimalUtils.isIntegerValue(maUser)) {
                return ResponseEntity.unprocessableEntity().body(ResNhanVienDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = nhanVienService.updateNhanVienById(id, hoTen, sdt, diaChi, maUser);

            if (result == 0) {
                return ResponseEntity.status(202).body(ResNhanVienDto.builder().code(202).message("Update Failed").build());
            }
            ResNhanVienDto response = ResNhanVienDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request updateNhanVienById: " + id, e);
            throw e;
        }
    }

    @PostMapping(value = "/{id}/delete")
    @ResponseBody
    public ResponseEntity<?> deleteNhanVienById(
            @PathVariable String id
    ) {
        logger.info("request deleteNhanVienById");
        try {
            int result = nhanVienService.deleteNhanVienById(id);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResNhanVienDto.builder().code(202).message("Delete Failed").build());
            }
            ResNhanVienDto response = ResNhanVienDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request deleteNhanVienById: " + id, e);
            throw e;
        }
    }

}
