package com.gara.api;

import com.gara.dto.ResDichVuDto;
import com.gara.model.DichVuModel;
import com.gara.service.DichVuService;
import com.gara.utils.BigDecimalUtils;
import com.gara.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/dichvu")
@AllArgsConstructor
public class DichVuController {
    private static Logger logger = LoggerFactory.getLogger(DichVuController.class);
    @Autowired
    private DichVuService dichVuService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> get(@PathVariable int id) {
        try {
            logger.info("#getDichVu - Start - id :" + id);
            List<DichVuModel> listDichVu = new ArrayList<DichVuModel>();
            DichVuModel dichVu = dichVuService.getDichVu(id);
            if (dichVu == null) {
                return ResponseEntity.status(404).body(ResDichVuDto.builder().code(404).message("Not Found").build());
            }
            listDichVu.add(dichVu);
            ResDichVuDto resDichVuDto = ResDichVuDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listDichVu)
                    .build();

            return ResponseEntity.ok().body(resDichVuDto);
        } catch (Exception e) {
            logger.info("request getDichVu: ", e);
            throw e;
        }
    }

    @GetMapping(value = "")
    public ResponseEntity<?> getAll() {
        try {
            logger.info("#getAllDichVu - Start:");
            List<DichVuModel> listDichVu = dichVuService.getDanhSachDichVu();
            ResDichVuDto resDichVuDto = ResDichVuDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listDichVu)
                    .build();
            logger.info("#getAllDichVu - Response:" + resDichVuDto.toString());
            return ResponseEntity.ok().body(resDichVuDto);
        } catch (Exception e) {
            logger.info("request getAllDichVu: ", e);
            throw e;
        }
    }

    @PostMapping(value = "/reg-info")
    @ResponseBody
    public ResponseEntity<?> regDichVuInfo(
            @RequestParam("tenDichVu") @NotBlank String tenDichVu,
            @RequestParam("tienCong") @NotNull String tienCong
    ) {
        logger.info("request regdichVuInfo");
        try {
            if (!BigDecimalUtils.isIntegerValue(tienCong)) {
                return ResponseEntity.unprocessableEntity().body(ResDichVuDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = dichVuService.insertDichVuInfo(tenDichVu, Integer.parseInt(tienCong));

            if (result == 0) {
                return ResponseEntity.status(202).body(ResDichVuDto.builder().code(202).message("Insert Failed").build());
            }

            ResDichVuDto response = ResDichVuDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request regDichVuInfo: " + tenDichVu + "," + tienCong, e);
            throw e;
        }
    }

    @PostMapping(value = "/update/{id}")
    @ResponseBody
    public ResponseEntity<?> updateDichVuById(
            @PathVariable String id,
            @RequestParam("tenDichVu") @NotBlank String tenDichVu,
            @RequestParam("tienCong") @NotNull String tienCong
    ) {
        logger.info("request updateDichVuById");
        try {
            if (!BigDecimalUtils.isIntegerValue(tienCong)) {
                return ResponseEntity.unprocessableEntity().body(ResDichVuDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = dichVuService.updateDichVuById(id, tenDichVu, Integer.parseInt(tienCong));

            if (result == 0) {
                return ResponseEntity.status(202).body(ResDichVuDto.builder().code(202).message("Update Failed").build());
            }
            ResDichVuDto response = ResDichVuDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request updateDichVuById: " + id, e);
            throw e;
        }
    }

    @PostMapping(value = "/{id}/delete")
    @ResponseBody
    public ResponseEntity<?> deleteDichVuById(
            @PathVariable String id
    ) {
        logger.info("request deleteDichVuById");
        try {
            int result = dichVuService.deleteDichVuById(id);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResDichVuDto.builder().code(202).message("Delete Failed").build());
            }
            ResDichVuDto response = ResDichVuDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request deleteDichVuById: " + id, e);
            throw e;
        }
    }

}
