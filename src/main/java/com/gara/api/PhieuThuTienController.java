package com.gara.api;

import com.gara.dto.ResPhieuSuaChuaDto;
import com.gara.dto.ResPhieuThuTienDto;
import com.gara.model.PhieuThuTienModel;
import com.gara.service.PhieuThuTienService;
import com.gara.utils.BigDecimalUtils;
import com.gara.utils.DateUtils;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/phieuthutien")
@AllArgsConstructor
public class PhieuThuTienController {
    private static Logger logger = LoggerFactory.getLogger(PhieuThuTienController.class);
    @Autowired
    private PhieuThuTienService phieuThuTienService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> get(@PathVariable String id) {
        try {
            logger.info("#getPhieuThuTien - Start - id :" + id);
            List<PhieuThuTienModel> listphieuThuTien = new ArrayList<PhieuThuTienModel>();
            PhieuThuTienModel phieuThuTien = phieuThuTienService.getPhieuThuTien(id);
            if (phieuThuTien == null) {
                return ResponseEntity.status(404).body(ResPhieuThuTienDto.builder().code(404).message("Not Found").build());
            }
            listphieuThuTien.add(phieuThuTien);
            ResPhieuThuTienDto resPhieuThuTienDto = ResPhieuThuTienDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listphieuThuTien)
                    .build();

            return ResponseEntity.ok().body(resPhieuThuTienDto);
        } catch (Exception e) {
            logger.info("request getPhieuThuTien: ", e);
            throw e;
        }
    }

    @GetMapping(value = "")
    public ResponseEntity<?> getAll() {
        try {
            logger.info("#getAllPhieuThuTien - Start:");
            List<PhieuThuTienModel> listphieuThuTien = phieuThuTienService.getDanhSachPhieuThuTien();
            ResPhieuThuTienDto resPhieuThuTienDto = ResPhieuThuTienDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listphieuThuTien)
                    .build();
            logger.info("#getAllPhieuThuTien - Response:" + resPhieuThuTienDto.toString());
            return ResponseEntity.ok().body(resPhieuThuTienDto);
        } catch (Exception e) {
            logger.info("request getAllPhieuThuTien: ", e);
            throw e;
        }
    }

    @PostMapping(value = "/reg-info")
    @ResponseBody
    public ResponseEntity<?> regPhieuThuTienInfo(
            @RequestParam("bienSo") @NotBlank String bienSo,
            @RequestParam("ngayThuTien") @NotBlank String ngayThuTien,
            @RequestParam("soTienThu") @NotNull String soTienThu,
            @RequestParam("maNV") @NotBlank String maNV
    ) {
        logger.info("request regphieuThuTienInfo");
        try {
            if (!DateUtils.validateDate(ngayThuTien) || !BigDecimalUtils.isIntegerValue(soTienThu) || !BigDecimalUtils.isIntegerValue(maNV)) {
                return ResponseEntity.unprocessableEntity().body(ResPhieuThuTienDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = phieuThuTienService.insertPhieuThuTienInfo(bienSo, ngayThuTien, Integer.parseInt(soTienThu), maNV);

            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhieuThuTienDto.builder().code(202).message("Insert Failed").build());
            }

            ResPhieuThuTienDto response = ResPhieuThuTienDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request regPhieuThuTienInfo: " + bienSo + "," + ngayThuTien + "," + soTienThu + "," + maNV, e);
            throw e;
        }
    }

    @PostMapping(value = "/update/{id}")
    @ResponseBody
    public ResponseEntity<?> updatePhieuThuTienById(
            @PathVariable String id,
            @RequestParam("bienSo") @NotBlank String bienSo,
            @RequestParam("ngayThuTien") @NotBlank String ngayThuTien,
            @RequestParam("soTienThu") @NotNull String soTienThu,
            @RequestParam("maNV") @NotBlank String maNV
    ) {
        logger.info("request updatePhieuThuTienById");
        try {
            if (!DateUtils.validateDate(ngayThuTien) || !BigDecimalUtils.isIntegerValue(soTienThu) || !BigDecimalUtils.isIntegerValue(maNV)) {
                return ResponseEntity.unprocessableEntity().body(ResPhieuThuTienDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = phieuThuTienService.updatePhieuThuTienById(id, bienSo, ngayThuTien, Integer.parseInt(soTienThu), maNV);

            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhieuThuTienDto.builder().code(202).message("Update Failed").build());
            }
            ResPhieuThuTienDto response = ResPhieuThuTienDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request updatePhieuThuTienById: " + id, e);
            throw e;
        }
    }

    @PostMapping(value = "/{id}/delete")
    @ResponseBody
    public ResponseEntity<?> deletephieuThuTienById(
            @PathVariable String id
    ) {
        logger.info("request deletephieuThuTienById");
        try {
            int result = phieuThuTienService.deletePhieuThuTienById(id);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhieuThuTienDto.builder().code(202).message("Delete Failed").build());
            }
            ResPhieuThuTienDto response = ResPhieuThuTienDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request deletePhieuThuTienById: " + id, e);
            throw e;
        }
    }

}
