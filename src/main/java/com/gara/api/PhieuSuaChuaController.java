package com.gara.api;

import com.gara.dto.ResPhieuSuaChuaDto;
import com.gara.model.ChiTietPhieuSuaChuaModel;
import com.gara.service.PhieuSuaChuaService;
import com.gara.utils.BigDecimalUtils;
import com.gara.utils.DateUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/phieusuachua")
@AllArgsConstructor
public class PhieuSuaChuaController {
    private static Logger logger = LoggerFactory.getLogger(PhieuSuaChuaController.class);
    @Autowired
    private PhieuSuaChuaService phieuSuaChuaService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> get(@PathVariable String id) {
        try {
            logger.info("#getChiTietPhieuSuaChua - Start - id :" + id);
            List<ChiTietPhieuSuaChuaModel> listphieuSuaChua = new ArrayList<ChiTietPhieuSuaChuaModel>();
            ChiTietPhieuSuaChuaModel phieuSuaChua = phieuSuaChuaService.getChiTietPhieuSuaChua(id);
            if (phieuSuaChua == null) {
                return ResponseEntity.status(404).body(ResPhieuSuaChuaDto.builder().code(404).message("Not Found").build());
            }
            listphieuSuaChua.add(phieuSuaChua);
            ResPhieuSuaChuaDto resPhieuSuaChuaDto = ResPhieuSuaChuaDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listphieuSuaChua)
                    .build();

            return ResponseEntity.ok().body(resPhieuSuaChuaDto);
        } catch (Exception e) {
            logger.info("request getChiTietPhieuSuaChua: ", e);
            throw e;
        }
    }

    @GetMapping(value = "")
    public ResponseEntity<?> getAll() {
        try {
            logger.info("#getAllChiTietPhieuSuaChua - Start:");
            List<ChiTietPhieuSuaChuaModel> listphieuSuaChua = phieuSuaChuaService.getDanhSachChiTietPhieuSuaChua();
            ResPhieuSuaChuaDto resPhieuSuaChuaDto = ResPhieuSuaChuaDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listphieuSuaChua)
                    .build();
            logger.info("#getAllChiTietPhieuSuaChua - Response:" + resPhieuSuaChuaDto.toString());
            return ResponseEntity.ok().body(resPhieuSuaChuaDto);
        } catch (Exception e) {
            logger.info("request getAllChiTietPhieuSuaChua: ", e);
            throw e;
        }
    }

    @PostMapping(value = "/reg-info")
    @ResponseBody
    public ResponseEntity<?> regPhieuSuaChuaInfo(
            @RequestParam("bienSo") @NotBlank String bienSo,
            @RequestParam("ngaySuaChua") @NotBlank String ngaySuaChua,
            @RequestParam("maNV") @NotBlank String maNV,
            @RequestParam("ghichu") String ghichu,
            @RequestParam("maDV") @NotBlank String maDV
    ) {
        logger.info("request regphieuSuaChuaInfo");
        try {
            if (!DateUtils.validateDate(ngaySuaChua) || !BigDecimalUtils.isIntegerValue(maNV)) {
                return ResponseEntity.unprocessableEntity().body(ResPhieuSuaChuaDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = phieuSuaChuaService.insertPhieuSuaChuaInfo(bienSo, ngaySuaChua, maNV, ghichu, maDV);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhieuSuaChuaDto.builder().code(202).message("Insert Failed").build());
            }

            ResPhieuSuaChuaDto response = ResPhieuSuaChuaDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request regPhieuSuaChuaInfo: " + bienSo + "," + ngaySuaChua + "," + maNV + "," + ghichu + "," + maDV, e);
            throw e;
        }
    }

    @PostMapping(value = "/update/{id}")
    @ResponseBody
    public ResponseEntity<?> updatePhieuSuaChuaById(
            @PathVariable String id,
            @RequestParam("bienSo") @NotBlank String bienSo,
            @RequestParam("ngaySuaChua") @NotBlank String ngaySuaChua,
            @RequestParam("maNV") @NotBlank String maNV,
            @RequestParam("maPSC") @NotBlank String maPSC,
            @RequestParam("ghichu") @NotBlank String ghichu,
            @RequestParam("maDV") @NotBlank String maDV
    ) {
        logger.info("request updatePhieuSuaChuaById");
        try {
            if (!DateUtils.validateDate(ngaySuaChua) || !BigDecimalUtils.isIntegerValue(maNV)) {
                return ResponseEntity.unprocessableEntity().body(ResPhieuSuaChuaDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = phieuSuaChuaService.updatePhieuSuaChuaById(id, bienSo, ngaySuaChua, maNV,maPSC,ghichu,maDV);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhieuSuaChuaDto.builder().code(202).message("Update Failed").build());
            }
            ResPhieuSuaChuaDto response = ResPhieuSuaChuaDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request updatePhieuSuaChuaById: " + id, e);
            throw e;
        }
    }

    @PostMapping(value = "/{id}/delete")
    @ResponseBody
    public ResponseEntity<?> deletePhieuSuaChuaById(
            @PathVariable String id
    ) {
        logger.info("request deletePhieuSuaChuaById");
        try {
            int result = phieuSuaChuaService.deletePhieuSuaChuaById(id);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhieuSuaChuaDto.builder().code(202).message("Delete Failed").build());
            }
            ResPhieuSuaChuaDto response = ResPhieuSuaChuaDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request deletePhieuSuaChuaById: " + id, e);
            throw e;
        }
    }

}
