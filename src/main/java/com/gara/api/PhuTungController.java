package com.gara.api;

import com.gara.dto.ResPhuTungDto;
import com.gara.model.PhuTungModel;
import com.gara.service.PhuTungService;
import com.gara.utils.BigDecimalUtils;
import com.gara.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/phutung")
@AllArgsConstructor
public class PhuTungController {
    private static Logger logger = LoggerFactory.getLogger(PhuTungController.class);
    @Autowired
    private PhuTungService phuTungService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> get(@PathVariable String id) {
        try {
            logger.info("#getPhuTung - Start - id :" + id);
            List<PhuTungModel> listPhuTung = new ArrayList<PhuTungModel>();
            PhuTungModel phuTung = phuTungService.getPhuTung(id);
            if (phuTung == null) {
                return ResponseEntity.status(404).body(ResPhuTungDto.builder().code(404).message("Not Found").build());
            }
            listPhuTung.add(phuTung);
            ResPhuTungDto resPhuTungDto = ResPhuTungDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listPhuTung)
                    .build();

            return ResponseEntity.ok().body(resPhuTungDto);
        } catch (Exception e) {
            logger.info("request getPhuTung: ", e);
            throw e;
        }
    }

    @GetMapping(value = "")
    public ResponseEntity<?> getAll() {
        try {
            logger.info("#getAllPhuTung - Start:");
            List<PhuTungModel> listPhuTung = phuTungService.getDanhSachPhuTung();
            ResPhuTungDto resPhuTungDto = ResPhuTungDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listPhuTung)
                    .build();
            logger.info("#getAllPhuTung - Response:" + resPhuTungDto.toString());
            return ResponseEntity.ok().body(resPhuTungDto);
        } catch (Exception e) {
            logger.info("request getAllPhuTung: ", e);
            throw e;
        }
    }

    @PostMapping(value = "/reg-info")
    @ResponseBody
    public ResponseEntity<?> regPhuTungInfo(
            @RequestParam("tenPhuTung") @NotBlank String tenPhuTung,
            @RequestParam("mauSac") @NotBlank String mauSac,
            @RequestParam("moTa") @NotNull String moTa,
            @RequestParam("giaNhap") @NotNull String giaNhap,
            @RequestParam("giaBan") @NotNull String giaBan,
            @RequestParam("maLoaiPT") @NotBlank String maLoaiPT
    ) {
        logger.info("request regphuTungInfo");
        try {
            if (!BigDecimalUtils.isIntegerValue(giaNhap) || !BigDecimalUtils.isIntegerValue(giaBan) || !BigDecimalUtils.isIntegerValue(maLoaiPT)) {
                return ResponseEntity.unprocessableEntity().body(ResPhuTungDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = phuTungService.insertPhuTungInfo(tenPhuTung, mauSac, moTa, Integer.parseInt(giaNhap), Integer.parseInt(giaBan), maLoaiPT);

            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhuTungDto.builder().code(202).message("Insert Failed").build());
            }

            ResPhuTungDto response = ResPhuTungDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request regPhuTungInfo: " + tenPhuTung + "," + mauSac + "," + moTa + "," + giaNhap + "," + giaBan + "," + maLoaiPT, e);
            throw e;
        }
    }

    @PostMapping(value = "/update/{id}")
    @ResponseBody
    public ResponseEntity<?> updatePhuTungById(
            @PathVariable String id,
            @RequestParam("tenPhuTung") @NotBlank String tenPhuTung,
            @RequestParam("mauSac") @NotBlank String mauSac,
            @RequestParam("moTa") @NotNull String moTa,
            @RequestParam("giaNhap") @NotNull String giaNhap,
            @RequestParam("giaBan") @NotNull String giaBan,
            @RequestParam("maLoaiPT") @NotBlank String maLoaiPT
    ) {
        logger.info("request updatePhuTungById");
        try {
            if (!BigDecimalUtils.isIntegerValue(giaNhap) || !BigDecimalUtils.isIntegerValue(giaBan) || !BigDecimalUtils.isIntegerValue(maLoaiPT)) {
                return ResponseEntity.unprocessableEntity().body(ResPhuTungDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = phuTungService.updatePhuTungById(id, tenPhuTung, mauSac, moTa, Integer.parseInt(giaNhap), Integer.parseInt(giaBan), maLoaiPT);

            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhuTungDto.builder().code(202).message("Update Failed").build());
            }
            ResPhuTungDto response = ResPhuTungDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request updatePhuTungById: " + id, e);
            throw e;
        }
    }

    @PostMapping(value = "/{id}/delete")
    @ResponseBody
    public ResponseEntity<?> deletePhuTungById(
            @PathVariable String id
    ) {
        logger.info("request deletePhuTungById");
        try {
            int result = phuTungService.deletePhuTungById(id);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResPhuTungDto.builder().code(202).message("Delete Failed").build());
            }
            ResPhuTungDto response = ResPhuTungDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request deletePhuTungById: " + id, e);
            throw e;
        }
    }

}
