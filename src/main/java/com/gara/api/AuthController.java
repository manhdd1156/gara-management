package com.gara.api;


import com.gara.dto.ResUserDto;
import com.gara.model.UserModel;
import com.gara.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/api/v1/user")
@Validated
public class AuthController {
	private static Logger logger = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	private UserService userService;


	@PostMapping(value = "/login")
	@ResponseBody
	public ResponseEntity<?> login(@RequestParam("username") @NotBlank String username, @RequestParam("password") @NotBlank String password) throws Exception {
		long start = System.currentTimeMillis();
		logger.info("request login: {}- {}", username,password);
		try {
			if ("".equals(username) || "".equals(password)) {
				return ResponseEntity.status(201).build();
			} else {
				UserModel response = userService.loginPassword(username, password);
				if(response == null) {
					return ResponseEntity.status(404).body(ResUserDto.builder().code(404).message("Not Found").build());
				}
				ResUserDto resDto =  ResUserDto.builder()
						.code(200)
						.message("Success")
						.info(response)
						.build();
				return ResponseEntity.ok().body(resDto);
			}

		} catch (Exception e) {
			logger.error("request login: " + username, e);
			throw e;
		}
	}
}
