package com.gara.api;

import com.gara.dto.ResKhachHangDto;
import com.gara.model.KhachHangModel;
import com.gara.service.KhachHangService;
import com.gara.utils.BigDecimalUtils;
import com.gara.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/khachhang")
@AllArgsConstructor
public class KhachHangController {
    private static Logger logger = LoggerFactory.getLogger(KhachHangController.class);
    @Autowired
    private KhachHangService khachHangService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> get(@PathVariable String id) {
        try {
            logger.info("#getKhachHang - Start - id :" + id);
            List<KhachHangModel> listKhachHang = new ArrayList<KhachHangModel>();
            KhachHangModel khachHang = khachHangService.getKhachHang(id);
            if (khachHang == null) {
                return ResponseEntity.status(404).body(ResKhachHangDto.builder().code(404).message("Not Found").build());
            }
            listKhachHang.add(khachHang);
            ResKhachHangDto resKhachHangDto = ResKhachHangDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listKhachHang)
                    .build();

            return ResponseEntity.ok().body(resKhachHangDto);
        } catch (Exception e) {
            logger.info("request getKhachHang: ", e);
            throw e;
        }
    }

    @GetMapping(value = "/search")
    public ResponseEntity<?> getByName(@RequestParam("hoTen") @NotBlank String hoTen) {
        try {
            logger.info("#getKhachHangByHoTen - Start :" + hoTen);
            List<KhachHangModel> listKhachHang = new ArrayList<KhachHangModel>();
            KhachHangModel khachHang = khachHangService.getKhachHangByHoTen(hoTen);
            if (khachHang == null) {
                return ResponseEntity.status(404).body(ResKhachHangDto.builder().code(404).message("Not Found").build());
            }
            listKhachHang.add(khachHang);
            ResKhachHangDto resKhachHangDto = ResKhachHangDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listKhachHang)
                    .build();

            return ResponseEntity.ok().body(resKhachHangDto);
        } catch (Exception e) {
            logger.info("request getKhachHangByHoTen: ", e);
            throw e;
        }
    }

    @GetMapping(value = "")
    public ResponseEntity<?> getAll() {
        try {
            logger.info("#getAllKhachHang - Start:");
            List<KhachHangModel> listKhachHang = khachHangService.getDanhSachKhachHang();
            ResKhachHangDto resKhachHangDto = ResKhachHangDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listKhachHang)
                    .build();
            logger.info("#getAllKhachHang - Response:" + resKhachHangDto.toString());
            return ResponseEntity.ok().body(resKhachHangDto);
        } catch (Exception e) {
            logger.info("request getAllKhachHang: ", e);
            throw e;
        }
    }

    @PostMapping(value = "/reg-info")
    @ResponseBody
    public ResponseEntity<?> regKhachHangInfo(
            @RequestParam("hoTen") @NotBlank String hoTen,
            @RequestParam("diaChi") @NotBlank String diaChi,
            @RequestParam("sdt") @NotNull String sdt,
            @RequestParam("email") @NotBlank String email
    ) {
        logger.info("request regkhachHangInfo");
        try {
            if (!StringUtils.checkPhone(sdt)|| !StringUtils.checkEmail(email)) {
                return ResponseEntity.unprocessableEntity().body(ResKhachHangDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = khachHangService.insertKhachHangInfo(hoTen, sdt, diaChi, email);

            if (result == 0) {
                return ResponseEntity.status(202).body(ResKhachHangDto.builder().code(202).message("Insert Failed").build());
            }

            ResKhachHangDto response = ResKhachHangDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request regKhachHangInfo: " + hoTen + "," + diaChi + "," + sdt + "," + email, e);
            throw e;
        }
    }

    @PostMapping(value = "/update/{id}")
    @ResponseBody
    public ResponseEntity<?> updateKhachHangById(
            @PathVariable String id,
            @RequestParam("hoTen") @NotBlank String hoTen,
            @RequestParam("diaChi") @NotBlank String diaChi,
            @RequestParam("sdt") @NotNull String sdt,
            @RequestParam("email") @NotBlank String email
    ) {
        logger.info("request updateKhachHangById");
        try {
            if (!StringUtils.checkPhone(sdt) || !StringUtils.checkEmail(email)) {
                return ResponseEntity.unprocessableEntity().body(ResKhachHangDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = khachHangService.updateKhachHangById(id, hoTen, sdt, diaChi, email);

            if (result == 0) {
                return ResponseEntity.status(202).body(ResKhachHangDto.builder().code(202).message("Update Failed").build());
            }
            ResKhachHangDto response = ResKhachHangDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request updateKhachHangById: " + id, e);
            throw e;
        }
    }

    @PostMapping(value = "/{id}/delete")
    @ResponseBody
    public ResponseEntity<?> deleteKhachHangById(
            @PathVariable String id
    ) {
        logger.info("request deleteKhachHangById");
        try {
            int result = khachHangService.deleteKhachHangById(id);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResKhachHangDto.builder().code(202).message("Delete Failed").build());
            }
            ResKhachHangDto response = ResKhachHangDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request deleteKhachHangById: " + id, e);
            throw e;
        }
    }

}
