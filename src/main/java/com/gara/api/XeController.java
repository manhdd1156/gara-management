package com.gara.api;

import com.gara.dto.ResXeDto;
import com.gara.model.XeModel;
import com.gara.service.XeService;
import com.gara.utils.BigDecimalUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/xe")
@AllArgsConstructor
public class XeController {
    private static Logger logger = LoggerFactory.getLogger(XeController.class);
    @Autowired
    private XeService xeService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> get(@PathVariable String id) {
        try {
            logger.info("#getXe - Start - id :" + id);
            List<XeModel> listXe = new ArrayList<XeModel>();
            XeModel xe = xeService.getXe(id);
            if (xe == null) {
                return ResponseEntity.status(404).body(ResXeDto.builder().code(404).message("Not Found").build());
            }
            listXe.add(xe);
            ResXeDto resXeDto = ResXeDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listXe)
                    .build();

            return ResponseEntity.ok().body(resXeDto);
        } catch (Exception e) {
            logger.info("request getXe: ", e);
            throw e;
        }
    }

    @GetMapping(value = "")
    public ResponseEntity<?> getAll() {
        try {
            logger.info("#getAllXe - Start:");
            List<XeModel> listXe = xeService.getDanhSachXe();
            ResXeDto resXeDto = ResXeDto.builder()
                    .code(200)
                    .message("Success")
                    .data(listXe)
                    .build();
            logger.info("#getAllXe - Response:" + resXeDto.toString());
            return ResponseEntity.ok().body(resXeDto);
        } catch (Exception e) {
            logger.info("request getAllXe: ", e);
            throw e;
        }
    }

    @PostMapping(value = "/reg-info")
    @ResponseBody
    public ResponseEntity<?> regXeInfo(
            @RequestParam("bienSo") @NotBlank String bienSo,
            @RequestParam("tenXe") @NotBlank String tenXe,
            @RequestParam("maHangXe") @NotNull String maHangXe,
            @RequestParam("tinhTrang") @NotNull String tinhTrang,
            @RequestParam("mauSac") @NotNull String mauSac,
            @RequestParam("maKH") @NotNull String maKH
    ) {
        logger.info("request regxeInfo");
        try {
            if (!BigDecimalUtils.isIntegerValue(maKH)) {
                return ResponseEntity.unprocessableEntity().body(ResXeDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = xeService.insertXeInfo(bienSo,tenXe,maHangXe,tinhTrang,mauSac, Integer.parseInt(maKH));
            if (result == 0) {
                return ResponseEntity.status(202).body(ResXeDto.builder().code(202).message("Insert Failed").build());
            }

            ResXeDto response = ResXeDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request regXeInfo: " + tenXe + "," + maKH, e);
            throw e;
        }
    }

    @PostMapping(value = "/update")
    @ResponseBody
    public ResponseEntity<?> updateXeByBienSo(
            @RequestParam("bienSo") @NotBlank String bienSo,
            @RequestParam("tenXe") @NotBlank String tenXe,
            @RequestParam("maHangXe") @NotNull String maHangXe,
            @RequestParam("tinhTrang") @NotNull String tinhTrang,
            @RequestParam("mauSac") @NotNull String mauSac,
            @RequestParam("maKH") @NotNull String maKH
    ) {
        logger.info("request updateXeByBienSo");
        try {
            if (!BigDecimalUtils.isIntegerValue(maKH)) {
                return ResponseEntity.unprocessableEntity().body(ResXeDto.builder().code(422).message("Unprocessable Entity").build());
            }
            int result = xeService.updateXeByBienSo(bienSo,tenXe,maHangXe,tinhTrang,mauSac, Integer.parseInt(maKH));

            if (result == 0) {
                return ResponseEntity.status(202).body(ResXeDto.builder().code(202).message("Update Failed").build());
            }
            ResXeDto response = ResXeDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request updateXeByBienSo: " + bienSo, e);
            throw e;
        }
    }

    @PostMapping(value = "/delete")
    @ResponseBody
    public ResponseEntity<?> deleteXeByBienSo(
            @RequestParam("bienSo") @NotBlank String bienSo
    ) {
        logger.info("request deleteXeByBienSo");
        try {
            int result = xeService.deleteXeByBienSo(bienSo);
            if (result == 0) {
                return ResponseEntity.status(202).body(ResXeDto.builder().code(202).message("Delete Failed").build());
            }
            ResXeDto response = ResXeDto.builder().code(200).message("Success").build();
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            logger.error("request deleteXeByBienSo: " + bienSo, e);
            throw e;
        }
    }

}
