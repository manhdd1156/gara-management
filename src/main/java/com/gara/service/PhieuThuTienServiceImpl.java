package com.gara.service;

import com.gara.dao.PhieuThuTienDao;
import com.gara.model.PhieuThuTienModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhieuThuTienServiceImpl implements PhieuThuTienService {

    @Autowired
    private PhieuThuTienDao phieuThuTienDao;

    @Override
    public List<PhieuThuTienModel> getDanhSachPhieuThuTien() {
        List<PhieuThuTienModel> list = new ArrayList<PhieuThuTienModel>();
        list = phieuThuTienDao.getDanhSachPhieuThuTien();
        return list;
    }

    @Override
    public PhieuThuTienModel getPhieuThuTien(String id) {
        return phieuThuTienDao.getPhieuThuTien(id);
    }

    @Override
    public int deletePhieuThuTienById(String id) {
        return phieuThuTienDao.deletePhieuThuTienById(id);
    }

    @Override
    public int updatePhieuThuTienById(String id, String bienSo, String ngayThuTien, int soTienThu, String maNV) {
        return phieuThuTienDao.updatePhieuThuTienById(id, bienSo, ngayThuTien, soTienThu, maNV);
    }

    @Override
    public int insertPhieuThuTienInfo(String bienSo, String ngayThuTien, int soTienThu, String maNV) {
        return phieuThuTienDao.insertPhieuThuTienInfo(bienSo, ngayThuTien, soTienThu, maNV);
    }


}
