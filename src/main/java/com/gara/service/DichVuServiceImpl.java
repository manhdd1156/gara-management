package com.gara.service;

import com.gara.dao.DichVuDao;
import com.gara.model.DichVuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DichVuServiceImpl implements DichVuService {

    @Autowired
    private DichVuDao phuTungDao;

    @Override
    public List<DichVuModel> getDanhSachDichVu() {
        List<DichVuModel> list = new ArrayList<DichVuModel>();
        list = phuTungDao.getDanhSachDichVu();
        return list;
    }

    @Override
    public DichVuModel getDichVu(int id) {
        return phuTungDao.getDichVu(id);
    }

    @Override
    public int deleteDichVuById(String id) {
        return phuTungDao.deleteDichVuById(id);
    }

    @Override
    public int updateDichVuById(String id, String tenDichVu, int tienCong) {
        return phuTungDao.updateDichVuById(id, tenDichVu, tienCong);
    }

    @Override
    public int insertDichVuInfo(String tenDichVu, int tienCong) {
        return phuTungDao.insertDichVuInfo(tenDichVu, tienCong);
    }


}
