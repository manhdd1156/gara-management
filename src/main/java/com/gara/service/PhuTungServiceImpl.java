package com.gara.service;

import com.gara.dao.PhuTungDao;
import com.gara.model.PhuTungModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhuTungServiceImpl implements PhuTungService {

    @Autowired
    private PhuTungDao phuTungDao;

    @Override
    public List<PhuTungModel> getDanhSachPhuTung() {
        List<PhuTungModel> list = new ArrayList<PhuTungModel>();
        list = phuTungDao.getDanhSachPhuTung();
        return list;
    }

    @Override
    public PhuTungModel getPhuTung(String id) {
        return phuTungDao.getPhuTung(id);
    }

    @Override
    public int deletePhuTungById(String id) {
        return phuTungDao.deletePhuTungById(id);
    }

    @Override
    public int updatePhuTungById(String id, String tenPhuTung, String mauSac, String moTa, int giaNhap, int giaBan, String maLoaiPT) {
        return phuTungDao.updatePhuTungById(id, tenPhuTung, mauSac, moTa, giaNhap, giaBan, maLoaiPT);
    }

    @Override
    public int insertPhuTungInfo(String tenPhuTung, String mauSac, String moTa, int giaNhap, int giaBan, String maLoaiPT) {
        return phuTungDao.insertPhuTungInfo(tenPhuTung, mauSac, moTa, giaNhap, giaBan, maLoaiPT);
    }


}
