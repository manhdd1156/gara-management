package com.gara.service;

import com.gara.dao.*;
import com.gara.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhieuSuaChuaServiceImpl implements PhieuSuaChuaService {

    @Autowired
    private PhieuSuaChuaDao phieuSuaChuaDao;

    @Autowired
    private XeDao xeDao;

    @Autowired
    private KhachHangDao khachHangDao;

    @Autowired
    private PhuTungDao phuTungDao;

    @Autowired
    private DichVuDao dichVuDao;

    @Override
    public List<ChiTietPhieuSuaChuaModel> getDanhSachChiTietPhieuSuaChua() {
        List<ChiTietPhieuSuaChuaModel> list = new ArrayList<ChiTietPhieuSuaChuaModel>();
        list = phieuSuaChuaDao.getDanhSachChiTietPhieuSuaChua();
        for (int i=0;i<list.size();i++) {
            PhieuSuaChuaModel phieuSuaChuaModel = phieuSuaChuaDao.getPhieuSuaChua(list.get(i).getMaPSC());
            XeModel xeModel = xeDao.getXe(phieuSuaChuaModel.getBienSo());
            KhachHangModel khachHangModel = khachHangDao.getKhachHang(xeModel.getMaKH());
            List<PhuTungCTPSCModel> listPhuTungCTPSCModel = phuTungDao.getDanhSachPhuTungByIdCTPSC(list.get(i).getId());
            DichVuModel dichVuModel = dichVuDao.getDichVu(list.get(i).getMaDV());

            double tongTien = dichVuModel.getTienCong();
            for(int j =0;j<listPhuTungCTPSCModel.size();j++) {
                tongTien+= listPhuTungCTPSCModel.get(j).getSoLuongSuDung() * listPhuTungCTPSCModel.get(j).getPhuTung().getGiaBan();
            }
            list.get(i).setPhieuSuaChua(phieuSuaChuaModel);
            list.get(i).setXe(xeModel);
            list.get(i).setKhachHang(khachHangModel);
            list.get(i).setPhuTungSuDung(listPhuTungCTPSCModel);
            list.get(i).setDichVu(dichVuModel);
            list.get(i).setTongTien(tongTien);
        }
        return list;
    }

    @Override
    public PhieuSuaChuaModel getPhieuSuaChua(String id) {
        return null;
    }

    @Override
    public List<PhieuSuaChuaModel> getDanhSachPhieuSuaChua() {
        return null;
    }

    @Override
    public ChiTietPhieuSuaChuaModel getChiTietPhieuSuaChua(String id) {
        ChiTietPhieuSuaChuaModel chiTietPhieuSuaChua = phieuSuaChuaDao.getChiTietPhieuSuaChua(id);

            PhieuSuaChuaModel phieuSuaChuaModel = phieuSuaChuaDao.getPhieuSuaChua(chiTietPhieuSuaChua.getMaPSC());
            XeModel xeModel = xeDao.getXe(phieuSuaChuaModel.getBienSo());
            KhachHangModel khachHangModel = khachHangDao.getKhachHang(xeModel.getMaKH());
            List<PhuTungCTPSCModel> listPhuTungCTPSCModel = phuTungDao.getDanhSachPhuTungByIdCTPSC(chiTietPhieuSuaChua.getId());
            DichVuModel dichVuModel = dichVuDao.getDichVu(chiTietPhieuSuaChua.getMaDV());

            double tongTien = dichVuModel.getTienCong();
            for(int j =0;j<listPhuTungCTPSCModel.size();j++) {
                tongTien+= listPhuTungCTPSCModel.get(j).getSoLuongSuDung() * listPhuTungCTPSCModel.get(j).getPhuTung().getGiaBan();
            }
            chiTietPhieuSuaChua.setPhieuSuaChua(phieuSuaChuaModel);
            chiTietPhieuSuaChua.setXe(xeModel);
            chiTietPhieuSuaChua.setKhachHang(khachHangModel);
            chiTietPhieuSuaChua.setPhuTungSuDung(listPhuTungCTPSCModel);
            chiTietPhieuSuaChua.setDichVu(dichVuModel);
            chiTietPhieuSuaChua.setTongTien(tongTien);

        return chiTietPhieuSuaChua;
    }

    @Override
    public int deletePhieuSuaChuaById(String id) {
        return phieuSuaChuaDao.deletePhieuSuaChuaById(id);
    }

    @Override
    public int updatePhieuSuaChuaById(String id, String bienSo, String ngaySuaChua, String maNV,String maPSC, String ghichu, String maDV) {
        return phieuSuaChuaDao.updatePhieuSuaChuaById(id, bienSo, ngaySuaChua, maNV,maPSC, ghichu, maDV);
    }
    @Override
    public int insertPhieuSuaChuaInfo(String bienSo, String ngaySuaChua, String maNV,String ghichu,String maDV) {
        return phieuSuaChuaDao.insertPhieuSuaChuaInfo(bienSo, ngaySuaChua, maNV, ghichu, maDV);
    }


}
