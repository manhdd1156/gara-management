package com.gara.service;


import com.gara.model.ChiTietPhieuSuaChuaModel;
import com.gara.model.PhieuSuaChuaModel;

import java.util.List;

public interface PhieuSuaChuaService {

    PhieuSuaChuaModel getPhieuSuaChua(String id);

    List<PhieuSuaChuaModel> getDanhSachPhieuSuaChua();

    ChiTietPhieuSuaChuaModel getChiTietPhieuSuaChua(String id);

    List<ChiTietPhieuSuaChuaModel> getDanhSachChiTietPhieuSuaChua();

    int deletePhieuSuaChuaById(String id);

    int updatePhieuSuaChuaById(String id, String bienSo, String ngaySuaChua, String maNV,String maPSC, String ghichu, String maDV);

    int insertPhieuSuaChuaInfo(String bienSo, String ngaySuaChua, String maNV,String ghichu,String maDV);
}
