package com.gara.service;


import com.gara.model.XeModel;

import java.util.List;

public interface XeService {

    XeModel getXe(String bienSo);

    List<XeModel> getDanhSachXe();

    int deleteXeByBienSo(String bienSo);

    int updateXeByBienSo(String bienSo, String tenXe, String maHangXe, String tinhTrang, String mauSac, int maKH);

    int insertXeInfo(String bienSo, String tenXe, String maHangXe, String tinhTrang, String mauSac, int maKH);
}
