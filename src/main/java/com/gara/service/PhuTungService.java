package com.gara.service;


import com.gara.model.PhuTungModel;

import java.util.List;

public interface PhuTungService {

    PhuTungModel getPhuTung(String id);

    List<PhuTungModel> getDanhSachPhuTung();

    int deletePhuTungById(String id);

    int updatePhuTungById(String id, String tenPhuTung, String mauSac, String moTa, int giaNhap, int giaBan, String maLoaiPT);

    int insertPhuTungInfo(String tenPhuTung, String mauSac, String moTa, int giaNhap, int giaBan, String maLoaiPT);
}
