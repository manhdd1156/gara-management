package com.gara.service;


import com.gara.model.KhachHangModel;
import com.gara.model.NhanVienModel;

import java.util.List;

public interface NhanVienService {

    NhanVienModel getNhanVien(String id);

    NhanVienModel getNhanVienByHoTen(String hoTen);

    List<NhanVienModel> getDanhSachNhanVien();

    int deleteNhanVienById(String id);

    int updateNhanVienById(String id, String hoTen, String sdt, String diaChi, String maNV);

    int insertNhanVienInfo(String hoTen, String sdt, String diaChi, String maNV);
}
