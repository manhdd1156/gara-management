package com.gara.service;

import com.gara.dao.XeDao;
import com.gara.model.XeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class XeServiceImpl implements XeService {

    @Autowired
    private XeDao xeDao;

    @Override
    public List<XeModel> getDanhSachXe() {
        List<XeModel> list = new ArrayList<XeModel>();
        list = xeDao.getDanhSachXe();
        return list;
    }

    @Override
    public XeModel getXe(String bienSo) {
        return xeDao.getXe(bienSo);
    }

    @Override
    public int deleteXeByBienSo(String bienSo) {
        return xeDao.deleteXeByBienSo(bienSo);
    }

    @Override
    public int updateXeByBienSo(String bienSo, String tenXe, String maHangXe, String tinhTrang, String mauSac, int maKH) {
        return xeDao.updateXeByBienSo(bienSo, tenXe, maHangXe, tinhTrang, mauSac, maKH);
    }

    @Override
    public int insertXeInfo(String bienSo, String tenXe, String maHangXe, String tinhTrang, String mauSac, int maKH) {
        return xeDao.insertXeInfo(bienSo, tenXe, maHangXe, tinhTrang, mauSac, maKH);
    }


}
