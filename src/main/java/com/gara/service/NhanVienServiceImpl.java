package com.gara.service;

import com.gara.dao.NhanVienDao;
import com.gara.model.KhachHangModel;
import com.gara.model.NhanVienModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NhanVienServiceImpl implements NhanVienService {

    @Autowired
    private NhanVienDao nhanVienDao;

    @Override
    public List<NhanVienModel> getDanhSachNhanVien() {
        List<NhanVienModel> list = new ArrayList<NhanVienModel>();
        list = nhanVienDao.getDanhSachNhanVien();
        return list;
    }

    @Override
    public NhanVienModel getNhanVien(String id) {
        return nhanVienDao.getNhanVien(id);
    }

    @Override
    public NhanVienModel getNhanVienByHoTen(String hoTen) {
        return nhanVienDao.getNhanVienByHoTen(hoTen);
    }

    @Override
    public int deleteNhanVienById(String id) {
        return nhanVienDao.deleteNhanVienById(id);
    }

    @Override
    public int updateNhanVienById(String id, String hoTen, String sdt,String diaChi, String maNV) {
        return nhanVienDao.updateNhanVienById(id, hoTen, sdt,diaChi, maNV);
    }

    @Override
    public int insertNhanVienInfo(String hoTen, String sdt,String diaChi, String maNV) {
        return nhanVienDao.insertNhanVienInfo(hoTen, sdt,diaChi, maNV);
    }


}
