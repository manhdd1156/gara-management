package com.gara.service;

import com.gara.dao.KhachHangDao;
import com.gara.model.KhachHangModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KhachHangServiceImpl implements KhachHangService {

    @Autowired
    private KhachHangDao khachHangDao;

    @Override
    public List<KhachHangModel> getDanhSachKhachHang() {
        List<KhachHangModel> list = new ArrayList<KhachHangModel>();
        list = khachHangDao.getDanhSachKhachHang();
        return list;
    }

    @Override
    public KhachHangModel getKhachHang(String id) {
        return khachHangDao.getKhachHang(id);
    }

    @Override
    public KhachHangModel getKhachHangByHoTen(String hoTen) {
        return khachHangDao.getKhachHangByHoTen(hoTen);
    }

    @Override
    public int deleteKhachHangById(String id) {
        return khachHangDao.deleteKhachHangById(id);
    }

    @Override
    public int updateKhachHangById(String id, String hoTen, String sdt,String diaChi, String email) {
        return khachHangDao.updateKhachHangById(id, hoTen, sdt,diaChi, email);
    }

    @Override
    public int insertKhachHangInfo(String hoTen, String sdt,String diaChi, String email) {
        return khachHangDao.insertKhachHangInfo(hoTen, sdt,diaChi, email);
    }


}
