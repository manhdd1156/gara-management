package com.gara.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChiTietPhieuSuaChuaModel {
    private Integer id;
    private int maPSC;
    private int maDV;
    private String ghichu;
    private double tongTien;
    private PhieuSuaChuaModel phieuSuaChua;
    private DichVuModel dichVu;
    private KhachHangModel khachHang;
    private XeModel xe;
    private List<PhuTungCTPSCModel> phuTungSuDung;
}
