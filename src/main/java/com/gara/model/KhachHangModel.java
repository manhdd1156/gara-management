package com.gara.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KhachHangModel {
    private Integer id;
    private String hoTen;
    private String sdt;
    private String diaChi;
    private String email;
}
