package com.gara.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhuTungCTPSCModel {
    private Integer id;
    private int soLuongSuDung;
    private Integer maCTPSC;
    private Integer maPT;
    private PhuTungModel phuTung;
}
