package com.gara.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhieuSuaChuaModel {
    private Integer id;
    private String bienSo;
    private Date ngaySuaChua;
    private Date ngayTao;
    private String maNV;
}
