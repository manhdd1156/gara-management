package com.gara.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhuTungModel {
    private int id;
    private String tenPhuTung;
    private String mauSac;
    private String moTa;
    private int giaNhap;
    private int giaBan;
    private int soluong;
    private String maLoaiPT;

}
