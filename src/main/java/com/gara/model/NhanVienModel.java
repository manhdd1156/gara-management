package com.gara.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NhanVienModel {
    private Integer id;
    private String hoTen;
    private String sdt;
    private String diaChi;
    private String maUser;
}
